﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Storage.Helper
{
    public class AzureConfig
    {
        public string ContainerName { get; set; }
        public string AccountName { get; set; }
        public string AccountKey { get; set; }
    }
}
