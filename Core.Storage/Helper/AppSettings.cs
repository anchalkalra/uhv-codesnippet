﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Storage.Helper
{
    public class AppSettings
    {
        public string ImageSize { get; set; }
        public string ImageQuality { get; set; }
        public bool IsCreateThumbnail { get; set; }
        public bool IsSaveOriginalFile { get; set; }
        public bool IsLocalSaveEnabled { get; set; }
        public bool IsS3SaveEnabled { get; set; }
        public bool IsAWS { get; set; }
    }
}
