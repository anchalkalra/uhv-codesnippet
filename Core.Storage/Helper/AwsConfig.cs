﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Storage.Helper
{
    public class AwsConfig
    {
        public string Region { get; set; }
        public string AWSAccessKey { get; set; }
        public string AWSSecretKey { get; set; }
        public string BucketName { get; set; }
        public string AmazonS3PublicUrl { get; set; }
    }
}
