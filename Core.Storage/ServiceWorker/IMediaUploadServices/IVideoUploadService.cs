﻿using System;
using System.Collections.Generic;
using System.Text;
using uhv.Customer.Model.Model.StorageModel;

namespace Core.Storage.ServiceWorker.IMediaUploadServices
{
    public interface IVideoUploadService
    {
        List<string> SaveVideo(MediaUploadView documents);
        List<ImageResponseView> SaveMultipleVideo(List<MediaUploadView> documents);
    }
}
