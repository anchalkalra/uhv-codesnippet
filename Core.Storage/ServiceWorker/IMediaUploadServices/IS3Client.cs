﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Core.Storage.ServiceWorker.IMediaUploadServices
{
    public interface IS3Client
    {
        bool UploadImageFile(string fileName, string pathToSave, Bitmap bmpObject, int imageQuality);
        bool UploadVideoFile(string fileName, string pathToSave, byte[] fileBytes);
        bool UploadFile(string fileName, string pathToSave, byte[] fileBytes);
    }
}
