﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Core.Storage.ServiceWorker.IMediaUploadServices
{
    public interface IS3AzureClient
    {
        bool UploadImageFileAzure(string fileName, string pathToSave, Bitmap bmpObject, int imageQuality);
        bool UploadVideoFileAzure(string fileName, string pathToSave, byte[] fileBytes);
    }
}
