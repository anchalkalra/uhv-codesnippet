﻿using System;
using System.Collections.Generic;
using System.Text;
using uhv.Customer.Model.Model.StorageModel;

namespace Core.Storage.ServiceWorker.IMediaUploadServices
{
    public interface IImageUploadService
    {
        ImageResponseView SaveImages(MediaUploadView documents);
        List<ImageResponseView> SaveMultipleImages(List<MediaUploadView> documents);
    }
}
