﻿using Common.Enum.StorageEnum;
using Core.Storage.Helper;
using Core.Storage.ServiceWorker.IMediaUploadServices;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using uhv.Customer.Model.Model.StorageModel;

namespace Core.Storage.ServiceWorker.MediaUploadServices
{
    public class UploadFileService : IUploadFileService
    {
       // private NLog.Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        public readonly IWebHostEnvironment _webHostEnvironment;
        private readonly AppSettings _appSettings;
        private readonly IS3Client _s3Client;
        private readonly IS3AzureClient _s3AzureClient;

        public UploadFileService(IOptions<AppSettings> appSettings, IS3Client s3Client, IS3AzureClient s3AzureClient)
        {
            _appSettings = appSettings.Value;
            _s3Client = s3Client;
            _s3AzureClient = s3AzureClient;
        }

        public List<string> UploadFile(MediaUploadView documents)
        {
            try
            {
                string timeSpan = new TimeSpan(DateTime.UtcNow.Ticks).ToString().Replace(":", "_").Replace(".", "_");
                string diretorycPath = string.Empty;
                string fileName = string.Empty;
                string filePrefixName = string.Empty;

                if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.SavePDF))
                {
                    filePrefixName = "Invoice_";
                    diretorycPath = "/Invoices" + "/" + documents.ReferenceId.ToString() + "/" + documents.CreatedBy.ToString();
                    if (documents.Documents.Count > 0)
                        fileName = documents.Documents[0].FileName + "_" + timeSpan + ".pdf";
                }
                List<string> generatedIds = new List<string>();
                if (documents.Documents.Count > 0)
                {
                    if (_appSettings.IsS3SaveEnabled)
                    {
                        SaveToS3(documents, diretorycPath, fileName, generatedIds);
                    }
                }
                return generatedIds;
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }

        private void SaveToS3(MediaUploadView documents, string diretorycPath, string fileName, List<string> generatedIds)
        {
            foreach (var item in documents.Documents)
            {
                if (item.Filebytes.Length > 0)
                {
                    if (_appSettings.IsAWS)
                    {
                        var fileGuid = Guid.NewGuid();
                        string desti = diretorycPath + "/" + fileName;
                        var IsUploadVideoFile = _s3Client.UploadFile(fileName, diretorycPath, item.Filebytes);

                        generatedIds.Add(System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/"));
                    }                   
                }
            }
        }
    }
}
