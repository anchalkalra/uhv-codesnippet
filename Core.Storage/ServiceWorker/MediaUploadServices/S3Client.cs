﻿using Amazon.S3;
using Amazon.S3.Model;
using Core.Storage.Helper;
using Core.Storage.ServiceWorker.IMediaUploadServices;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Core.Storage.ServiceWorker.MediaUploadServices
{
    public class S3Client : IS3Client
    {
        private AwsConfig _awsConfig { get; set; }
        // private AzureConfig _azureConfig { get; set; }
        private IAmazonS3 _s3Client { get; set; }

        public S3Client(IOptions<AwsConfig> awsConfig, IAmazonS3 s3Client) //, IOptions<AzureConfig> azureConfig) 
        {
            _awsConfig = awsConfig.Value;
            _s3Client = s3Client;
            // _azureConfig = azureConfig.Value;
        }
        public bool UploadImageFile(string fileName, string pathToSave, Bitmap bmpObject, int imageQuality)
        {
            bool result = false;
            try
            {
                System.Drawing.Imaging.EncoderParameters encoderParams = new System.Drawing.Imaging.EncoderParameters();
                System.Drawing.Imaging.EncoderParameter encoderParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, imageQuality);
                encoderParams.Param[0] = encoderParam;
                System.Drawing.Imaging.ImageCodecInfo[] arrayICI = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
                for (int fwd = 0; fwd <= arrayICI.Length - 1; fwd++)
                {
                    if (arrayICI[fwd].FormatDescription.Equals("JPEG"))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            //NewBMP.Save(PathToSave + FileName, arrayICI[fwd], encoderParams);
                            bmpObject.Save(memoryStream, arrayICI[fwd], encoderParams);

                            var request = new PutObjectRequest()
                            {
                                BucketName = string.Concat(_awsConfig.BucketName, pathToSave),
                                CannedACL = S3CannedACL.PublicRead,//PERMISSION TO FILE PUBLIC ACCESIBLE
                                Key = string.Format(fileName),
                                InputStream = memoryStream//SEND THE FILE STREAM
                            };

                            var response = _s3Client.PutObjectAsync(request).Result;
                            if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                            {
                                result = true;
                            }
                        }
                        break;
                    }
                }
                return result;
            } 
            catch
            { 
                throw;
            }
        }
         
        public bool UploadVideoFile(string fileName, string pathToSave, byte[] fileBytes)
        {
            bool result = false;
            try
            {
                using (var memoryStream = new MemoryStream(fileBytes))
                {

                    var request = new PutObjectRequest()
                    {
                        BucketName = string.Concat(_awsConfig.BucketName, pathToSave),
                        CannedACL = S3CannedACL.PublicRead,//PERMISSION TO FILE PUBLIC ACCESIBLE
                        Key = string.Format(fileName),
                        InputStream = memoryStream //SEND THE FILE STREAM
                    };

                    var response = _s3Client.PutObjectAsync(request).Result;
                    if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                    {
                        result = true;
                    }
                    return result;
                }
            } 
            catch
            {
                throw;
            }
        }
        public bool UploadFile(string fileName, string pathToSave, byte[] fileBytes)
        {
            bool result = false;
            try
            {
                using (var memoryStream = new MemoryStream(fileBytes))
                {

                    var request = new PutObjectRequest()
                    {
                        BucketName = string.Concat(_awsConfig.BucketName, pathToSave),
                        CannedACL = S3CannedACL.PublicRead,//PERMISSION TO FILE PUBLIC ACCESIBLE
                        Key = string.Format(fileName),
                        InputStream = memoryStream //SEND THE FILE STREAM
                    };

                    var response = _s3Client.PutObjectAsync(request).Result;
                    if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                    {
                        result = true;
                    }
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

    }
}
