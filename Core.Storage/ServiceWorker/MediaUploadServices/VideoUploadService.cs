﻿using Common.Enum.StorageEnum;
using Core.Storage.Helper;
using Core.Storage.ServiceWorker.IMediaUploadServices;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using uhv.Customer.Model.Model.StorageModel;

namespace Core.Storage.ServiceWorker.MediaUploadServices
{
    public class VideoUploadService : IVideoUploadService
    {
        //static StringBuilder sbMessage = new StringBuilder();
        private NLog.Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        public readonly IWebHostEnvironment _webHostEnvironment;
        private readonly AppSettings _appSettings;
        private readonly IS3Client _s3Client;
        private readonly IS3AzureClient _s3AzureClient;

        public VideoUploadService(IWebHostEnvironment webHostEnvironment, IOptions<AppSettings> appSettings, IS3Client s3Client, IS3AzureClient s3AzureClient)
        {
            _webHostEnvironment = webHostEnvironment;
            _appSettings = appSettings.Value;
            _s3Client = s3Client;
            _s3AzureClient = s3AzureClient;
        }

        public List<string> SaveVideo(MediaUploadView documents)
        {
            try
            {
                string timeSpan = new TimeSpan(DateTime.UtcNow.Ticks).ToString().Replace(":", "_").Replace(".", "_");
                string diretorycPath = string.Empty;
                string fileName = string.Empty;
                string filePrefixName = string.Empty;

                if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Contest))
                {
                    filePrefixName = "Contest_";
                    diretorycPath = "/ContestVideos";
                    fileName = "Contest_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                }
                else if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.ContestQuestion))
                {
                    filePrefixName = "Contest_";
                    diretorycPath = "/ContestVideos";
                    fileName = "ContestQues_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                }
                else if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.QuestionOption))
                {
                    filePrefixName = "Contest_";
                    diretorycPath = "/ContestVideos";
                    fileName = "QuesOption_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                }
                else if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Content))
                {
                    filePrefixName = "Content_";
                    diretorycPath = "/ContentVideos";
                    fileName = "Content_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                }
                //else if (documents.ReferenceType == Convert.ToInt32(ReferenceTypeEnum.Post))
                //{
                //    _logger.LogWarning("Inside Video Upload service - Creating filename for post Video Upload");

                //    filePrefixName = "Post_";
                //    diretorycPath = "/Post/Video";
                //    fileName = "Post_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                //    //tFileName = "TPost_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".flv";
                //}

                //else if (documents.ReferenceType == Convert.ToInt32(ReferenceTypeEnum.PlaceHolder))
                //{
                //    filePrefixName = "PlaceHolder";
                //    if (documents.ModuleId.Equals(Convert.ToInt64(PlaceHolderEnum.MyStore)))
                //    {
                //        diretorycPath = "/PlaceHolders/MyStore";
                //    }
                //    else if (documents.ModuleId.Equals(Convert.ToInt64(PlaceHolderEnum.Transact)))
                //    {
                //        diretorycPath = "/PlaceHolders/Transact";
                //    }
                //    else if (documents.ModuleId.Equals(Convert.ToInt64(PlaceHolderEnum.StoreBuilder)))
                //    {
                //        diretorycPath = "/PlaceHolders/StoreBuilder";
                //    }
                //    else if (documents.ModuleId.Equals(Convert.ToInt64(PlaceHolderEnum.Wellness)))
                //    {
                //        diretorycPath = "/PlaceHolders/Wellness";
                //    }

                //    fileName = "PlaceHolder_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                //}

                if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Banners))
                {
                    filePrefixName = "Banner_";
                    diretorycPath = "/BannerImages/Video";
                    fileName = "Banner_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                }

                //string filesName = filesPrefixName + documents.ReferenceId.ToString() + ".flv"; ;
                List<string> generatedIds = new List<string>();
                if (documents.Documents.Count > 0)
                {
                    if (_appSettings.IsLocalSaveEnabled)
                    {
                        SaveToLocalDisk(documents, diretorycPath, fileName, generatedIds);
                    }
                    if (_appSettings.IsS3SaveEnabled)
                    {
                        SaveToS3(documents, diretorycPath, fileName, generatedIds);
                    }
                }
                return generatedIds;
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }


        public List<ImageResponseView> SaveMultipleVideo(List<MediaUploadView> documents)
        {
            try
            {
                ImageResponseView imageResponse = new ImageResponseView();
                List<ImageResponseView> imageResponseView = new List<ImageResponseView>();
                //sbMessage.AppendLine("Enter SaveVideo start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                foreach (MediaUploadView media in documents)
                {
                    string timeSpan = new TimeSpan(DateTime.UtcNow.Ticks).ToString().Replace(":", "_").Replace(".", "_");
                    string diretorycPath = string.Empty;
                    string fileName = string.Empty;
                    string filePrefixName = string.Empty;

                    if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Contest))
                    {
                        filePrefixName = "Contest_";
                        diretorycPath = "/ContestVideos";
                        fileName = "Contest_" + media.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                    }
                    else if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.ContestQuestion))
                    {
                        filePrefixName = "Contest_";
                        diretorycPath = "/ContestVideos";
                        fileName = "ContestQues_" + media.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                    }
                    else if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.QuestionOption))
                    {
                        filePrefixName = "Contest_";
                        diretorycPath = "/ContestVideos";
                        fileName = "QuesOption_" + media.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                    }
                    else if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Content))
                    {
                        filePrefixName = "Content_";
                        diretorycPath = "/ContentVideos";
                        fileName = "Content_" + media.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                    }

                    if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Banners))
                    {
                        filePrefixName = "Banner_";
                        diretorycPath = "/BannerImages/Video";
                        fileName = "Banner_" + media.ReferenceId.ToString() + "_" + timeSpan + ".mp4";
                    }

                    List<string> generatedIds = new List<string>();
                    if (media.Documents.Count > 0)
                    {
                        if (_appSettings.IsLocalSaveEnabled)
                        {
                            //sbMessage.AppendLine("Enter SaveToLocalDisk start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                            imageResponse = SaveMultipleToLocalDisk(media, diretorycPath, fileName, generatedIds);
                            //sbMessage.AppendLine("End SaveToLocalDisk start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                        }
                        if (_appSettings.IsS3SaveEnabled)
                        {
                            //sbMessage.AppendLine("Enter SaveToS3 start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                            imageResponse = SaveMultipleToS3(media, diretorycPath, fileName, generatedIds);
                            //sbMessage.AppendLine("End SaveToS3 start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                        }
                        imageResponse.MediaTypeId = media.MediaTypeId;
                        imageResponse.ExternalLink = media.ExternalLink;
                        imageResponse.Sequence = media.Sequence;
                        imageResponseView.Add(imageResponse);
                    }
                }
                //sbMessage.AppendLine("End SaveVideo start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                return imageResponseView;
            }
            catch
            {
                throw;
            }
            finally
            {
                //System.IO.File.WriteAllText("C:\\Temp\\VideoServiceLog.txt", sbMessage.ToString());
            }
        }


        private void SaveToS3(MediaUploadView documents, string diretorycPath, string fileName, List<string> generatedIds)
        {
            foreach (var item in documents.Documents)
            {
                if (item.Filebytes.Length > 0)
                {
                    if (_appSettings.IsAWS)
                    {
                        var fileGuid = Guid.NewGuid();
                        string desti = diretorycPath + "/" + fileName;
                        var IsUploadVideoFile = _s3Client.UploadVideoFile(fileName, diretorycPath, item.Filebytes);

                        generatedIds.Add(System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/"));
                    }
                    else
                    {
                        var fileGuid = Guid.NewGuid();
                        string desti = diretorycPath + "/" + fileName;
                        var IsUploadVideoFile = _s3AzureClient.UploadVideoFileAzure(fileName, diretorycPath, item.Filebytes);
                        generatedIds.Add(System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/"));
                    }
                }
            }
        }

        private ImageResponseView SaveMultipleToS3(MediaUploadView documents, string diretorycPath, string fileName, List<string> generatedIds)
        {
            ImageResponseView imageResponseView = new ImageResponseView();
            foreach (var item in documents.Documents)
            {
                if (item.Filebytes.Length > 0)
                {
                    if (_appSettings.IsAWS)
                    {
                        var fileGuid = Guid.NewGuid();
                        string desti = diretorycPath + "/" + fileName;
                        var IsUploadVideoFile = _s3Client.UploadVideoFile(fileName, diretorycPath, item.Filebytes);
                        imageResponseView.SavedPathList = new List<string>() { System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/") };
                    }
                    else
                    {
                        var fileGuid = Guid.NewGuid();
                        string desti = diretorycPath + "/" + fileName;
                        var IsUploadVideoFile = _s3AzureClient.UploadVideoFileAzure(fileName, diretorycPath, item.Filebytes);
                        imageResponseView.SavedPathList = new List<string>() { System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/") };
                    }
                    //generatedIds.Add(System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/"));
                }
            }

            return imageResponseView;
        }
        public void SaveToLocalDisk(MediaUploadView documents, string diretorycPath, string fileName, List<string> generatedIds)
        {
            foreach (var item in documents.Documents)
            {
                if (item.Filebytes.Length > 0)
                {
                    MemoryStream memoryStream = new MemoryStream(item.Filebytes);
                    var fileGuid = Guid.NewGuid();
                    string desti = diretorycPath + "\\" + fileName;

                    string destPath = _webHostEnvironment.WebRootPath + "\\" + diretorycPath + "\\\\" + fileName;
                    logger.Info("_hostingEnvironment.WebRootPath" + _webHostEnvironment.WebRootPath);
                    if (!Directory.Exists(_webHostEnvironment.WebRootPath + "\\" + diretorycPath))
                    {
                        Directory.CreateDirectory(_webHostEnvironment.WebRootPath + "\\" + diretorycPath);
                    }
                    IFormFile file = new FormFile(memoryStream, 0, item.Filebytes.Length, "", fileName);
                    using (FileStream fs = File.Create(destPath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    generatedIds.Add(System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/"));
                }
            }
        }

        public ImageResponseView SaveMultipleToLocalDisk(MediaUploadView documents, string diretorycPath, string fileName, List<string> generatedIds)
        {
            ImageResponseView imageResponseView = new ImageResponseView();
            foreach (var item in documents.Documents)
            {
                if (item.Filebytes.Length > 0)
                {
                    imageResponseView = new ImageResponseView();
                    MemoryStream memoryStream = new MemoryStream(item.Filebytes);
                    var fileGuid = Guid.NewGuid();
                    string desti = diretorycPath + "\\" + fileName;

                    string destPath = _webHostEnvironment.WebRootPath + "\\" + diretorycPath + "\\\\" + fileName;
                    logger.Info("_hostingEnvironment.WebRootPath" + _webHostEnvironment.WebRootPath);
                    if (!Directory.Exists(_webHostEnvironment.WebRootPath + "\\" + diretorycPath))
                    {
                        Directory.CreateDirectory(_webHostEnvironment.WebRootPath + "\\" + diretorycPath);
                    }
                    IFormFile file = new FormFile(memoryStream, 0, item.Filebytes.Length, "", fileName);
                    using (FileStream fs = File.Create(destPath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    //generatedIds.Add(System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/"));
                    imageResponseView.SavedPathList = new List<string>() { System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/") };
                }
            }
            return imageResponseView;
        }

    }
}
