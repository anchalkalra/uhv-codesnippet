﻿using Azure;
using Azure.Storage.Blobs;
using Core.Storage.Helper;
using Core.Storage.ServiceWorker.IMediaUploadServices;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Core.Storage.ServiceWorker.MediaUploadServices
{
    public class S3AzureClient : IS3AzureClient
    {
        //NLog.Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
        private AzureConfig _azureConfig { get; set; }
        public S3AzureClient(IOptions<AzureConfig> azureConfig)
        {
            _azureConfig = azureConfig.Value;
        }

        public bool UploadImageFileAzure(string fileName, string pathToSave, Bitmap bmpObject, int imageQuality)
        {
            bool result = false;
            try
            {
                System.Drawing.Imaging.EncoderParameters encoderParams = new System.Drawing.Imaging.EncoderParameters();
                System.Drawing.Imaging.EncoderParameter encoderParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, imageQuality);
                encoderParams.Param[0] = encoderParam;
                System.Drawing.Imaging.ImageCodecInfo[] arrayICI = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
                for (int fwd = 0; fwd <= arrayICI.Length - 1; fwd++)
                {
                    if (arrayICI[fwd].FormatDescription.Equals("JPEG"))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            bmpObject.Save(memoryStream, arrayICI[fwd], encoderParams);
                            var storageCredentials = new StorageCredentials(_azureConfig.AccountName, _azureConfig.AccountKey);
                            var cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);
                            var cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
                            string connectionString = Environment.GetEnvironmentVariable("AZURE_STORAGE_CONNECTION_STRING", EnvironmentVariableTarget.Machine);
                            BlobServiceClient blobServiceClient = new BlobServiceClient(connectionString);
                            BlobContainerClient container = new BlobContainerClient(connectionString, string.Concat(_azureConfig.ContainerName, pathToSave));

                            container.SetAccessPolicyAsync(Azure.Storage.Blobs.Models.PublicAccessType.Blob);
                            try
                            {
                                memoryStream.Position = 0;
                                BlobClient blob = container.GetBlobClient(fileName);
                                using (var fileStream = memoryStream)
                                {
                                    var response = blob.Upload(fileStream);
                                }
                                var blobString = blob.Uri.ToString();
                                if (!string.IsNullOrEmpty(blobString))
                                {
                                    result = true;
                                }

                            }
                            catch (Exception ex) {
                                throw ex;
                            }
                        }
                        break;
                    }
                }
                return result;
            }
            catch (RequestFailedException ex)
            {
                //logger.Error(ex, "AzureBlobException in AzureBlobClient");
                throw ex;
            }
            catch (Exception ex)
            {
                //logger.Error(ex, "General Exception in S3Client");
                throw;
            }
        }

        public bool UploadVideoFileAzure(string fileName, string pathToSave, byte[] fileBytes)
        {
            bool result = false;
            try
            {
                using (var memoryStream = new MemoryStream(fileBytes))
                {

                    var storageCredentials = new StorageCredentials(_azureConfig.AccountName, _azureConfig.AccountKey);
                    var cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);
                    var cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
                    string connectionString = Environment.GetEnvironmentVariable("AZURE_STORAGE_CONNECTION_STRING", EnvironmentVariableTarget.Machine);
                    BlobServiceClient blobServiceClient = new BlobServiceClient(connectionString);
                    BlobContainerClient container = new BlobContainerClient(connectionString, string.Concat(_azureConfig.ContainerName, pathToSave));

                    container.SetAccessPolicyAsync(Azure.Storage.Blobs.Models.PublicAccessType.Blob);

                    memoryStream.Position = 0;
                    BlobClient blob = container.GetBlobClient(fileName);
                    using (var fileStream = memoryStream)
                    {
                        var response = blob.Upload(fileStream);
                    }
                    var blobString = blob.Uri.ToString();
                    if (!string.IsNullOrEmpty(blobString))
                    {
                        result = true;
                    }
                    return result;
                }
            }
            catch (RequestFailedException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}