﻿using Common.Enum.StorageEnum;
using Core.Storage.Helper;
using Core.Storage.ServiceWorker.IMediaUploadServices;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using uhv.Customer.Model.Model.StorageModel;

namespace Core.Storage.ServiceWorker.MediaUploadServices
{
    public class ImageUploadService : IImageUploadService
    {
        public readonly IWebHostEnvironment _webHostEnvironment;
        private readonly AppSettings _appSettings;
        private readonly IS3Client _s3Client;
        private readonly IS3AzureClient _s3AzureClient;

        public ImageUploadService(IWebHostEnvironment webHostEnvironment, IOptions<AppSettings> appSettings, IS3Client s3Client, IS3AzureClient s3AzureClient)
        {
            _webHostEnvironment = webHostEnvironment;
            _appSettings = appSettings.Value;
            _s3Client = s3Client;
            _s3AzureClient = s3AzureClient;
        }

        /// <summary>
        /// Save Profile Picture details
        /// </summary>
        /// <param name="UserProfileUpload"></param>
        /// <returns>Response</returns>
        public ImageResponseView SaveImages(MediaUploadView documents)
        {
            ImageResponseView imageResponseView = new ImageResponseView();
            try
            {
                string timeSpan = new TimeSpan(DateTime.UtcNow.Ticks).ToString().Replace(":", "_").Replace(".", "_");
                string diretorycPath = string.Empty;
                string fileName = string.Empty;
                string tFileName = string.Empty;
                string filePrefixName = string.Empty;
                int size = Convert.ToInt32(_appSettings.ImageSize);
                int quality = Convert.ToInt32(_appSettings.ImageQuality);

                if (documents.ReferenceType == Convert.ToInt64(MediaReferenceEnum.Member))
                {
                    filePrefixName = "Profile_";
                    diretorycPath = "/ProfileImages";

                    fileName = "Profile_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    tFileName = "TProfile_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";

                }
                if (documents.ReferenceType == Convert.ToInt64(MediaReferenceEnum.ProfileCover))
                {
                    filePrefixName = "ProfileCover_";
                    diretorycPath = "/ProfileCoverImages";

                    fileName = "ProfileCover_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    tFileName = "TProfileCover_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";

                }
                else if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Contest))
                {
                    filePrefixName = "Contest_";
                    diretorycPath = "/ContestImages";
                    fileName = "Contest_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    tFileName = "TContest_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                }
                else if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.ContestQuestion))
                {
                    filePrefixName = "Contest_";
                    diretorycPath = "/ContestImages";
                    fileName = "ContestQues_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    tFileName = "TContestQues_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                }
                else if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.QuestionOption))
                {
                    filePrefixName = "Contest_";
                    diretorycPath = "/ContestImages";
                    fileName = "QuesOption_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    tFileName = "TQuesOption_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                }
                else if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Content))
                {
                    filePrefixName = "Content_";
                    diretorycPath = "/ContentImages";
                    fileName = "Content_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    tFileName = "TContent_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                }
                else if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.QRGenerator))
                {
                    filePrefixName = "QRCode_";
                    diretorycPath = "/QRCode" + "/" + documents.ReferenceId.ToString() + "/" + documents.CreatedBy.ToString();
                    //diretorycPath = "/QRCode";
                    if (documents.Documents.Count > 0)
                        fileName = documents.Documents[0].FileName + "_" + timeSpan + ".png";
                    tFileName = "TContent_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                }
                else if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.SavePDF))
                {
                    filePrefixName = "Invoice_";
                    diretorycPath = "/Invoices" + "/" + documents.ReferenceId.ToString() + "/" + documents.CreatedBy.ToString();
                    //diretorycPath = "/QRCode";
                    if (documents.Documents.Count > 0)
                        fileName = documents.Documents[0].FileName + "_" + timeSpan + ".pdf";
                    tFileName = "TContent_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".pdf";
                }
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.Scheme))
                //{
                //    filePrefixName = "Scheme_";
                //    diretorycPath = "/SchemeImages";
                //    fileName = "Scheme_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TScheme_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.Event))
                //{
                //    filePrefixName = "Event_";
                //    diretorycPath = "/EventImages";
                //    fileName = "Event_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TEvent_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.Consumer))
                //{
                //    filePrefixName = "Consumer_";
                //    diretorycPath = "/ProfileImages";

                //    fileName = "Consumer_" + documents.ReferenceId.ToString() + ".png";
                //    tFileName = "TConsumer_" + documents.ReferenceId.ToString() + ".png";
                //}
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.Servey))
                //{
                //    filePrefixName = "Survey_";
                //    diretorycPath = "/Survey";
                //    fileName = "Survey_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TSurvey_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.Poll))
                //{
                //    filePrefixName = "Poll_";
                //    diretorycPath = "/Survey";
                //    fileName = "Poll_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TPoll_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.Advertisement))
                //{
                //    filePrefixName = "Advertisement_";
                //    diretorycPath = "/Advertisement";
                //    fileName = "Advertisement_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TAdvertisement_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.Post))
                //{
                //    filePrefixName = "Post_";
                //    diretorycPath = "/Post";
                //    fileName = "Post_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TPost_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.ScratchWin))
                //{
                //    filePrefixName = "ScratchWin_";
                //    diretorycPath = "/ScratchWin";
                //    fileName = "ScratchWin_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TScratchWin_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.SofyxPointsProgram))
                //{
                //    filePrefixName = "Program_";
                //    diretorycPath = "/SPProgram";
                //    fileName = "Program_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TProgram_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.SofyxPointsPrize))
                //{
                //    filePrefixName = "Prize_";
                //    diretorycPath = "/SPPrize";
                //    fileName = "Prize_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TPrize_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.Store))
                //{
                //    filePrefixName = "Store_";
                //    diretorycPath = "/StoreImages";

                //    fileName = "Store_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TStore_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.Repaircenter))
                //{
                //    filePrefixName = "Repaircenter_";
                //    diretorycPath = "/RepaircenterImages";
                //    fileName = "Repaircenter_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TRepaircenter_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                else if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Banners))
                {
                    filePrefixName = "Banner_";
                    diretorycPath = "/BannerImages";
                    fileName = "Banner_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    tFileName = "TBanner_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                }
                //else if (documents.ReferenceType == Convert.ToInt32(ModuleEnum.SofyxLoyalityReward))
                //{
                //    filePrefixName = "SofyxLoyalityReward_";
                //    diretorycPath = "/SofyxLoyalityRewardImages";
                //    fileName = "SofyxLoyalityReward_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //    tFileName = "TSofyxLoyalityReward_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                //}
                if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Group))
                {
                    filePrefixName = "Group_";
                    diretorycPath = "/GroupImages";
                    fileName = "Group_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    tFileName = "TGroup_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                }
                else if (documents.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Placeholder))
                {
                    filePrefixName = "PlaceHolder";

                    if (documents.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.ContestImage)))
                    {
                        diretorycPath = "/PlaceHolders/ContestDefaultImage";
                    }
                    else if (documents.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.ContestThumbImage)))
                    {
                        diretorycPath = "/PlaceHolders/ContestDefaultThumbImage";
                    }
                    else if (documents.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.GroupImage)))
                    {
                        diretorycPath = "/PlaceHolders/GroupDefaultImage";
                    }
                    else if (documents.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.ProfileImage)))
                    {
                        diretorycPath = "/PlaceHolders/ProfileDefaultImage";
                    }
                    else if (documents.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.ProfileCoverImage)))
                    {
                        diretorycPath = "/PlaceHolders/ProfileCoverDefaultImage";
                    }
                    else if (documents.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.QuizImage)))
                    {
                        diretorycPath = "/PlaceHolders/QuizDefaultImage";
                    }

                    fileName = "PlaceHolder_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    tFileName = "TPlaceHolder_" + documents.ReferenceId.ToString() + "_" + timeSpan + ".png";
                }

                //string filesName = filesPrefixName + documents.ReferenceId.ToString() + ".png"; ;
                List<string> generatedIds = new List<string>();
                if (documents.Documents.Count > 0)
                {
                    if (_appSettings.IsLocalSaveEnabled)
                    {
                        SaveToLocalDisk(documents, imageResponseView, diretorycPath, fileName, quality, tFileName, size);
                    }
                    if (_appSettings.IsS3SaveEnabled)
                    {
                        SaveToS3(documents, imageResponseView, diretorycPath, fileName, quality);
                    }
                }
                return imageResponseView;
            }
            catch
            {
                throw;
            }
        }

        public List<ImageResponseView> SaveMultipleImages(List<MediaUploadView> documents)
        {
            ImageResponseView imageResponse = new ImageResponseView();
            List<ImageResponseView> imageResponseView = new List<ImageResponseView>();
            try
            {
                //sbMessage.AppendLine("Enter SaveImages start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                foreach (MediaUploadView media in documents)
                {
                    string timeSpan = new TimeSpan(DateTime.UtcNow.Ticks).ToString().Replace(":", "_").Replace(".", "_");
                    string diretorycPath = string.Empty;
                    string fileName = string.Empty;
                    string tFileName = string.Empty;
                    string filePrefixName = string.Empty;
                    int size = Convert.ToInt32(_appSettings.ImageSize);
                    int quality = Convert.ToInt32(_appSettings.ImageQuality);
                    if (media.ReferenceType == Convert.ToInt64(MediaReferenceEnum.Member))
                    {
                        filePrefixName = "Profile_";
                        diretorycPath = "/ProfileImages";

                        fileName = "Profile_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                        tFileName = "TProfile_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";

                    }
                    if (media.ReferenceType == Convert.ToInt64(MediaReferenceEnum.ProfileCover))
                    {
                        filePrefixName = "ProfileCover_";
                        diretorycPath = "/ProfileCoverImages";

                        fileName = "ProfileCover_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                        tFileName = "TProfileCover_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";

                    }
                    else if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Contest))
                    {
                        filePrefixName = "Contest_";
                        diretorycPath = "/ContestImages";
                        fileName = "Contest_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                        tFileName = "TContest_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    }
                    else if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.ContestQuestion))
                    {
                        filePrefixName = "Contest_";
                        diretorycPath = "/ContestImages";
                        fileName = "ContestQues_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                        tFileName = "TContestQues_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    }
                    else if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.QuestionOption))
                    {
                        filePrefixName = "Contest_";
                        diretorycPath = "/ContestImages";
                        fileName = "QuesOption_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                        tFileName = "TQuesOption_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    }
                    else if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Content))
                    {
                        filePrefixName = "Content_";
                        diretorycPath = "/ContentImages";
                        fileName = "Content_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                        tFileName = "TContent_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    }
                    else if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Banners))
                    {
                        filePrefixName = "Banner_";
                        diretorycPath = "/BannerImages";
                        fileName = "Banner_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                        tFileName = "TBanner_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    }
                    if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Group))
                    {
                        filePrefixName = "Group_";
                        diretorycPath = "/GroupImages";
                        fileName = "Group_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                        tFileName = "TGroup_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    }
                    else if (media.ReferenceType == Convert.ToInt32(MediaReferenceEnum.Placeholder))
                    {
                        filePrefixName = "PlaceHolder";

                        if (media.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.ContestImage)))
                        {
                            diretorycPath = "/PlaceHolders/ContestDefaultImage";
                        }
                        else if (media.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.ContestThumbImage)))
                        {
                            diretorycPath = "/PlaceHolders/ContestDefaultThumbImage";
                        }
                        else if (media.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.GroupImage)))
                        {
                            diretorycPath = "/PlaceHolders/GroupDefaultImage";
                        }
                        else if (media.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.ProfileImage)))
                        {
                            diretorycPath = "/PlaceHolders/ProfileDefaultImage";
                        }
                        else if (media.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.ProfileCoverImage)))
                        {
                            diretorycPath = "/PlaceHolders/ProfileCoverDefaultImage";
                        }
                        else if (media.ModuleId.Equals(Convert.ToInt64(PlaceHolderCategoryEnum.QuizImage)))
                        {
                            diretorycPath = "/PlaceHolders/QuizDefaultImage";
                        }

                        fileName = "PlaceHolder_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                        tFileName = "TPlaceHolder_" + media.ReferenceId.ToString() + "_" + timeSpan + ".png";
                    }

                    //string filesName = filesPrefixName + documents.ReferenceId.ToString() + ".png"; ;
                    List<string> generatedIds = new List<string>();
                    if (media.Documents.Count > 0)
                    {
                        if (_appSettings.IsLocalSaveEnabled)
                        {
                            //sbMessage.AppendLine("Enter SaveToLocalDisk start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                            imageResponse = SaveMultipleToLocalDisk(media, diretorycPath, fileName, quality, tFileName, size);
                            //sbMessage.AppendLine("End SaveToLocalDisk start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                        }
                        if (_appSettings.IsS3SaveEnabled)
                        {
                            //sbMessage.AppendLine("Enter SaveToS3 start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                            imageResponse = SaveMultipleToS3(media, diretorycPath, fileName, quality);
                            //sbMessage.AppendLine("End SaveToS3 start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                        }
                        imageResponse.MediaTypeId = media.MediaTypeId;
                        imageResponse.ExternalLink = media.ExternalLink;
                        imageResponse.Sequence = media.Sequence;
                        imageResponseView.Add(imageResponse);
                    }
                }
                //sbMessage.AppendLine("End SaveImages start at " + DateTime.Now.ToString("hh.mm.ss.ffffff") + "\r\n");
                return imageResponseView;

            }
            catch
            {
                throw;
            }
            finally
            {
                //System.IO.File.WriteAllText("C:\\Temp\\ImageServiceLog.txt", sbMessage.ToString());
            }
        }

        private void SaveToS3(MediaUploadView documents, ImageResponseView imageResponseView, string diretorycPath, string fileName, int quality)
        {
            foreach (var item in documents.Documents)
            {
                if (item.Filebytes.Length > 0)
                {
                    var fileGuid = Guid.NewGuid();
                    string desti = $@"{diretorycPath}/{fileName}";
                    string destPath = $@"{diretorycPath}/{fileName}";

                    item.FileExtenstion = "image/png";
                    item.FileName = fileName;
                    ImageResizedResponseView imageSaveResult = null;
                    imageSaveResult = Resize_Picture(item.Filebytes, fileName, diretorycPath, 0, 0, quality, isLocalSave: false);
                    imageResponseView.SavedPathList = new List<string>() { System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/") };
                    if (imageSaveResult != null)
                    {
                        imageResponseView.ImageHeight = imageSaveResult.ImageHeight;
                        imageResponseView.ImageWidth = imageSaveResult.ImageWidth;
                    }
                }
            }
        }

        public void SaveToLocalDisk(MediaUploadView documents, ImageResponseView imageResponseView, string diretorycPath, string fileName, int quality, string tFileName, int size)
        {
            foreach (var item in documents.Documents)
            {
                if (item.Filebytes.Length > 0)
                {
                    //System.IO.File.WriteAllText(@"C:\temp\LocalDisk.txt", "Enter into Method");
                    var fileGuid = Guid.NewGuid();
                    string desti = Path.Combine(diretorycPath, fileName);

                    string destPath = Path.Combine(_webHostEnvironment.WebRootPath, diretorycPath, fileName);
                    if (!Directory.Exists(Path.Combine(_webHostEnvironment.WebRootPath, diretorycPath)))
                    {
                        Directory.CreateDirectory(Path.Combine(_webHostEnvironment.WebRootPath, diretorycPath));
                    }

                    item.FileExtenstion = "image/png";
                    item.FileName = fileName;

                    var tempPath = $@"{_webHostEnvironment.WebRootPath}/{diretorycPath}/";
                    var imageSaveResult = Resize_Picture(item.Filebytes, fileName, tempPath, 0, 0, quality, isLocalSave: true);
                    //var imageSaveResult = Resize_Picture(item.Filebytes, fileName, Path.Combine(Path.Combine(_webHostEnvironment.WebRootPath, diretorycPath),"//"), 0, 0, quality, isLocalSave: true);
                    ImageResize(tempPath, fileName, tFileName, size, quality);
                    imageResponseView.SavedPathList = new List<string>() { desti };// { System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/") };
                    if (imageSaveResult != null)
                    {
                        imageResponseView.ImageHeight = imageSaveResult.ImageHeight;
                        imageResponseView.ImageWidth = imageSaveResult.ImageWidth;
                    }
                }
            }
        }

        public ImageResponseView SaveMultipleToLocalDisk(MediaUploadView documents, string diretorycPath, string fileName, int quality, string tFileName, int size)
        {
            ImageResponseView imageResponseView = new ImageResponseView();
            foreach (var item in documents.Documents)
            {
                if (item.Filebytes.Length > 0)
                {
                    imageResponseView = new ImageResponseView();
                    //System.IO.File.WriteAllText(@"C:\temp\LocalDisk.txt", "Enter into Method");
                    var fileGuid = Guid.NewGuid();
                    string desti = diretorycPath + "\\" + fileName;

                    string destPath = _webHostEnvironment.WebRootPath + "\\" + diretorycPath + "\\\\" + fileName;
                    if (!Directory.Exists(_webHostEnvironment.WebRootPath + "\\" + diretorycPath))
                    {
                        Directory.CreateDirectory(_webHostEnvironment.WebRootPath + "\\" + diretorycPath);
                    }

                    item.FileExtenstion = "image/png";
                    item.FileName = fileName;

                    var imageSaveResult = Resize_Picture(item.Filebytes, fileName, _webHostEnvironment.WebRootPath + "\\" + diretorycPath + "\\", 0, 0, quality, isLocalSave: true);
                    ImageResize(_webHostEnvironment.WebRootPath + "\\" + diretorycPath, fileName, tFileName, size, quality);
                    imageResponseView.SavedPathList = new List<string>() { System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/") };
                    if (imageSaveResult != null)
                    {
                        imageResponseView.ImageHeight = imageSaveResult.ImageHeight;
                        imageResponseView.ImageWidth = imageSaveResult.ImageWidth;
                    }
                }
            }
            return imageResponseView;
        }

        private ImageResponseView SaveMultipleToS3(MediaUploadView documents, string diretorycPath, string fileName, int quality)
        {
            ImageResponseView imageResponseView = new ImageResponseView();
            foreach (var item in documents.Documents)
            {
                if (item.Filebytes.Length > 0)
                {
                    imageResponseView = new ImageResponseView();
                    var fileGuid = Guid.NewGuid();
                    string desti = diretorycPath + "/" + fileName;
                    string destPath = diretorycPath + "/" + fileName;

                    item.FileExtenstion = "image/png";
                    item.FileName = fileName;
                    ImageResizedResponseView imageSaveResult = null;
                    imageSaveResult = Resize_Picture(item.Filebytes, fileName, diretorycPath, 0, 0, quality, isLocalSave: false);
                    imageResponseView.SavedPathList = new List<string>() { System.Text.RegularExpressions.Regex.Replace(desti, "\\\\", "/") };
                    if (imageSaveResult != null)
                    {
                        imageResponseView.ImageHeight = imageSaveResult.ImageHeight;
                        imageResponseView.ImageWidth = imageSaveResult.ImageWidth;
                    }
                }
            }
            return imageResponseView;
        }

        public ImageResizedResponseView Resize_Picture(byte[] Filebytes, string FileName, string PathToSave, int FinalWidth, int FinalHeight, int ImageQuality, bool isLocalSave)
        {
            ImageResizedResponseView imageResizedResponseView = new ImageResizedResponseView();
            System.Drawing.Bitmap NewBMP;
            System.Drawing.Graphics graphicTemp;

            using (Stream stream = new MemoryStream(Filebytes, false))
            {
                using (Image img = Image.FromStream(stream))
                {
                    if (isLocalSave)
                    {
                        img.Save($@"{PathToSave}Original_{FileName}");
                    }

                    foreach (var prop in img.PropertyItems)
                    {
                        if (prop.Id == 0x0112) //value of EXIF
                        {
                            int orientationValue = img.GetPropertyItem(prop.Id).Value[0];
                            RotateFlipType rotateFlipType = GetOrientationToFlipType(orientationValue);
                            img.RotateFlip(rotateFlipType);
                            break;
                        }
                    }
                    System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(img);
                    int iWidth;
                    int iHeight;
                    if (img.Height > 1920)
                    {
                        imageResizedResponseView.IsImageResized = true;
                        FinalHeight = 1920;
                        if ((FinalHeight == 0) && (FinalWidth != 0))
                        {
                            iWidth = FinalWidth;
                            iHeight = (bmp.Size.Height * iWidth / bmp.Size.Width);
                        }
                        else if ((FinalHeight != 0) && (FinalWidth == 0))
                        {
                            iHeight = FinalHeight;
                            iWidth = (bmp.Size.Width * iHeight / bmp.Size.Height);
                        }
                        else
                        {
                            iWidth = FinalWidth;
                            iHeight = FinalHeight;
                        }
                    }
                    else
                    {
                        FinalHeight = img.Height;
                        if ((FinalHeight == 0) && (FinalWidth != 0))
                        {
                            imageResizedResponseView.IsImageResized = true;
                            iWidth = FinalWidth;
                            iHeight = (bmp.Size.Height * iWidth / bmp.Size.Width);
                        }
                        else if ((FinalHeight != 0) && (FinalWidth == 0))
                        {
                            imageResizedResponseView.IsImageResized = true;
                            iHeight = FinalHeight;
                            iWidth = (bmp.Size.Width * iHeight / bmp.Size.Height);
                        }
                        else
                        {
                            iWidth = FinalWidth;
                            iHeight = FinalHeight;
                        }
                    }
                    NewBMP = new System.Drawing.Bitmap(iWidth, iHeight);
                    graphicTemp = System.Drawing.Graphics.FromImage(NewBMP);
                    graphicTemp.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                    graphicTemp.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    graphicTemp.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    graphicTemp.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphicTemp.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    graphicTemp.DrawImage(bmp, 0, 0, iWidth, iHeight);
                    graphicTemp.Dispose();
                    System.Drawing.Imaging.EncoderParameters encoderParams = new System.Drawing.Imaging.EncoderParameters();
                    System.Drawing.Imaging.EncoderParameter encoderParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, ImageQuality);
                    encoderParams.Param[0] = encoderParam;
                    System.Drawing.Imaging.ImageCodecInfo[] arrayICI = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();

                    if (isLocalSave)
                    {
                        for (int fwd = 0; fwd <= arrayICI.Length - 1; fwd++)
                        {
                            if (arrayICI[fwd].FormatDescription.Equals("JPEG"))
                            {
                                NewBMP.Save(PathToSave + FileName, arrayICI[fwd], encoderParams);
                                imageResizedResponseView.IsImageSaved = true;
                                imageResizedResponseView.ImageHeight = NewBMP.Height;
                                imageResizedResponseView.ImageWidth = NewBMP.Width;
                            }
                        }
                    }
                    else
                    {
                        if (_appSettings.IsAWS)
                        {
                            var isImageUploaded = _s3Client.UploadImageFile(FileName, PathToSave, NewBMP, ImageQuality);
                            if (isImageUploaded)
                            {
                                imageResizedResponseView.IsImageSaved = true;
                                imageResizedResponseView.ImageHeight = NewBMP.Height;
                                imageResizedResponseView.ImageWidth = NewBMP.Width;
                                if (_appSettings.IsCreateThumbnail)
                                {
                                    int size = Convert.ToInt32(_appSettings.ImageSize);
                                    FileName = "T" + FileName;
                                    SaveThumbnail(NewBMP, PathToSave, FileName, size, ImageQuality);
                                }
                            }
                        }
                        else
                        {
                            var isImageUploaded = _s3AzureClient.UploadImageFileAzure(FileName, PathToSave, NewBMP, ImageQuality);
                            if (isImageUploaded)
                            {
                                imageResizedResponseView.IsImageSaved = true;
                                imageResizedResponseView.ImageHeight = NewBMP.Height;
                                imageResizedResponseView.ImageWidth = NewBMP.Width;
                                if (_appSettings.IsCreateThumbnail)
                                {
                                    int size = Convert.ToInt32(_appSettings.ImageSize);
                                    FileName = "T" + FileName;
                                    SaveThumbnail(NewBMP, PathToSave, FileName, size, ImageQuality);
                                }
                            }
                        }
                    }

                    NewBMP.Dispose();
                    bmp.Dispose();
                }
            }

            return imageResizedResponseView;
        }
        public void ReduceImageSizeAndSave(byte[] Filebytes, string inputPath, string fileName, string outFileName, int size, int quality)
        {
            try
            {

                using (Stream stream = new MemoryStream(Filebytes, false))
                {
                    using (Image img = Image.FromStream(stream))
                    {
                        var orignalImageFormat = img.RawFormat;
                        foreach (var prop in img.PropertyItems)
                        {
                            if (prop.Id == 0x0112) //value of EXIF
                            {
                                int orientationValue = img.GetPropertyItem(prop.Id).Value[0];
                                RotateFlipType rotateFlipType = GetOrientationToFlipType(orientationValue);
                                img.RotateFlip(rotateFlipType);
                                break;
                            }
                        }
                        using (var image = new Bitmap(img))
                        {
                            int width = image.Width, height = image.Height;
                            var resized = new Bitmap(width, height);
                            using (var graphics = Graphics.FromImage(resized))
                            {
                                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                graphics.CompositingMode = CompositingMode.SourceCopy;
                                graphics.DrawImage(image, 0, 0, width, height);
                                using (var output = File.Open(Path.Combine(inputPath, outFileName), FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                                {
                                    var qualityParamId = System.Drawing.Imaging.Encoder.Quality;
                                    var encoderParameters = new EncoderParameters(1);
                                    encoderParameters.Param[0] = new EncoderParameter(qualityParamId, 0L);
                                    var codec = ImageCodecInfo.GetImageDecoders().FirstOrDefault(im => im.FormatID == ImageFormat.Png.Guid);
                                    resized.Save(output, orignalImageFormat);
                                    output.Close();
                                    output.Dispose();
                                    graphics.Dispose();
                                    resized.Dispose();
                                }
                            }
                            image.Dispose();
                        }
                        img.Dispose();
                    }
                }
            }
            catch
            {
                throw;
            }
            //return inputPath;
        }
        public void ImageResize(string inputPath, string fileName, string outFileName, int size, int quality)
        {
            try
            {
                using (System.Drawing.Image img = System.Drawing.Image.FromFile(Path.Combine(inputPath, fileName)))
                {
                    foreach (var prop in img.PropertyItems)
                    {
                        if (prop.Id == 0x0112) //value of EXIF
                        {
                            int orientationValue = img.GetPropertyItem(prop.Id).Value[0];
                            RotateFlipType rotateFlipType = GetOrientationToFlipType(orientationValue);
                            img.RotateFlip(rotateFlipType);
                            break;
                        }
                    }
                    using (var image = new Bitmap(img))
                    {

                        int width, height;
                        if (image.Width > image.Height)
                        {
                            width = size;
                            height = Convert.ToInt32(image.Height * size / (double)image.Width);
                        }
                        else
                        {
                            width = Convert.ToInt32(image.Width * size / (double)image.Height);
                            height = size;
                        }
                        var resized = new Bitmap(width, height);
                        using (var graphics = Graphics.FromImage(resized))
                        {
                            graphics.CompositingQuality = CompositingQuality.HighSpeed;
                            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            graphics.CompositingMode = CompositingMode.SourceCopy;
                            graphics.DrawImage(image, 0, 0, width, height);
                            using (var output = File.Open(Path.Combine(inputPath, outFileName), FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                            {
                                var qualityParamId = System.Drawing.Imaging.Encoder.Quality;
                                var encoderParameters = new EncoderParameters(1);
                                encoderParameters.Param[0] = new EncoderParameter(qualityParamId, quality);
                                var codec = ImageCodecInfo.GetImageDecoders().FirstOrDefault(im => im.FormatID == ImageFormat.Png.Guid);
                                resized.Save(output, codec, encoderParameters);
                                output.Close();
                                output.Dispose();
                                graphics.Dispose();
                                resized.Dispose();
                            }
                        }
                        image.Dispose();
                    }
                    img.Dispose();
                }
            }
            catch
            {
                throw;
            }
            //return inputPath;
        }
        private static RotateFlipType GetOrientationToFlipType(int orientationValue)
        {
            RotateFlipType rotateFlipType = RotateFlipType.RotateNoneFlipNone;

            switch (orientationValue)
            {
                case 1:
                    rotateFlipType = RotateFlipType.RotateNoneFlipNone;
                    break;
                case 2:
                    rotateFlipType = RotateFlipType.RotateNoneFlipX;
                    break;
                case 3:
                    rotateFlipType = RotateFlipType.Rotate180FlipNone;
                    break;
                case 4:
                    rotateFlipType = RotateFlipType.Rotate180FlipX;
                    break;
                case 5:
                    rotateFlipType = RotateFlipType.Rotate90FlipX;
                    break;
                case 6:
                    rotateFlipType = RotateFlipType.Rotate90FlipNone;
                    break;
                case 7:
                    rotateFlipType = RotateFlipType.Rotate270FlipX;
                    break;
                case 8:
                    rotateFlipType = RotateFlipType.Rotate270FlipNone;
                    break;
                default:
                    rotateFlipType = RotateFlipType.RotateNoneFlipNone;
                    break;
            }

            return rotateFlipType;
        }

        public bool SaveThumbnail(Bitmap bitmapObject, string pathToSave, string fileName, int size, int quality)
        {
            bool response = false;
            int width, height;
            if (bitmapObject.Width > bitmapObject.Height)
            {
                width = size;
                height = Convert.ToInt32(bitmapObject.Height * size / (double)bitmapObject.Width);
            }
            else
            {
                width = Convert.ToInt32(bitmapObject.Width * size / (double)bitmapObject.Height);
                height = size;
            }
            var resized = new Bitmap(width, height);
            using (var graphics = Graphics.FromImage(resized))
            {
                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.DrawImage(bitmapObject, 0, 0, width, height);
                var isImageUploaded = _s3Client.UploadImageFile(fileName, pathToSave, bitmapObject, quality);
                if (isImageUploaded)
                {
                    response = true;
                }
            }
            bitmapObject.Dispose();
            return response;
        }
    }
}

