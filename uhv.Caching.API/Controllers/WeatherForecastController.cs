﻿using Core.Caching;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uhv.Caching.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly IDistributedCacheService _cache;
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IDistributedCacheService cache)
        {
            _logger = logger;
            _cache = cache;
        }

        [HttpGet]
        public async Task<IEnumerable<WeatherForecast>> Get()
        {
            WeatherForecast[] dd;
            //byte[] cachedData = await _cache.GetAsync("test");
            //if (cachedData != null)
            //{
            //    var cachedDataString = Encoding.UTF8.GetString(cachedData);
            //    dd = Newtonsoft.Json.JsonConvert.DeserializeObject<WeatherForecast[]>(cachedDataString);
            //}
            //else
            //{
            //    var rng = new Random();
            //    dd = Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //    {
            //        Date = DateTime.Now.AddDays(index),
            //        TemperatureC = rng.Next(-20, 55),
            //        Summary = Summaries[rng.Next(Summaries.Length)]
            //    })
            //    .ToArray();

            //    await _cache.SetAsync("test", Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(dd)));

            //}
            //await _cache.RemoveCacheAsync("test");
            dd = await _cache.GetCacheAsync<WeatherForecast[]>("test");
            if (dd == null)
            {
                var rng = new Random();
                dd = Enumerable.Range(1, 5).Select(index => new WeatherForecast
                {
                    Date = DateTime.Now.AddDays(index),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                })
                .ToArray();

                await _cache.AddOrUpdateCacheAsync("test", dd);

            }
            return dd;
        }
    }
}
