﻿using Confluent.Kafka;
using Core.Caching;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace uhv.Caching.API
{
    public class KafkaBackgroundServices : BackgroundService
    {
        private readonly IServiceProvider serviceProvider;

        private ConsumerConfig consumerConfig;

        public KafkaBackgroundServices(ConsumerConfig consumerConfig, IServiceProvider serviceProvider)
        {
            this.consumerConfig = consumerConfig;
            this.serviceProvider = serviceProvider;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // This will start the WebAPI, removing it will make API stuck in current state and API will start only after getting the first data from Kafka consumer.
            await Task.Yield();

            // TODO: Pick topic name from appsettings.json
            using (IServiceScope serviceScope = serviceProvider.CreateScope())
            {
                IDistributedCacheService cache = serviceScope.ServiceProvider.GetRequiredService<IDistributedCacheService>();

                //ConsumerWrapper consumer = new ConsumerWrapper(this.consumerConfig, "NewRegistration");
                //await consumer.ReadAndProcessMessage((data) =>
                //{
                //    try
                //    {
                //        cache.AddOrUpdateCacheAsync("NewRegistration", data);
                //    }
                //    catch (Exception ex)
                //    {

                //        throw;
                //    }

                //}, stoppingToken);
            }
        }
    }
}
