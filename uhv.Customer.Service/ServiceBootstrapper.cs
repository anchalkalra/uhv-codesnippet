﻿using Amazon.S3;
using Common.Model;
using Core.Storage.Helper;
using Core.Storage.ServiceWorker.IMediaUploadServices;
using Core.Storage.ServiceWorker.MediaUploadServices;
using Logger;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Utilities;
using Utility;
using uhv.Customer.Model;
using uhv.Customer.Service.Infrastructure.Repository;
using uhv.Customer.Service.ServiceWorker;
using Core.Email.EmailNotification;
using uhv.Customer.Model.Model;
using PaymentService;

namespace uhv.Customer.Service
{
    public class ServiceBootstrapper
    {

        static ServiceBootstrapper()
        {

        }
        /// <summary>
        /// To Configure services at a common place
        /// </summary>
        /// <param name="services">All service collection</param>
        /// <param name="defaultConnection">default connection string parameter</param>
        public static void ConfigureServices(IServiceCollection services, IConfiguration Configuration, string defaultConnection = "")
        {
            services.Configure<CustomerBaseUrlsConfig>(Configuration.GetSection("urls"));
            // Set Configuration
            services.Configure<AuthenticationConfig>(Configuration.GetSection("AuthenticationConfig"));
            services.Configure<ResetPasswordConfig>(Configuration.GetSection("ResetPassword"));
            services.Configure<SendVerificationMailConfig>(Configuration.GetSection("EmailVerification"));
            services.Configure<SMSConfig>(Configuration.GetSection("SMSConfig"));
            services.Configure<EmailSenderConfig>(Configuration.GetSection("EmailProvider"));
            
            var awsCommon = Configuration.GetSection("Common");
            if (awsCommon != null)
            {
                services.Configure<CommonConfig>(Configuration.GetSection("Common"));
            }
            // Configuration for Storage
            var awsConfig = Configuration.GetSection("AWS");
            var storageConfig = Configuration.GetSection("Storage:AppSettings");
            if (awsConfig != null && storageConfig !=null)
            {
                services.Configure<AwsConfig>(Configuration.GetSection("AWS"));
                services.Configure<AzureConfig>(Configuration.GetSection("Storage:AZURE"));
                services.Configure<AppSettings>(Configuration.GetSection("Storage:AppSettings"));
                services.AddDefaultAWSOptions(Configuration.GetAWSOptions());
                services.AddAWSService<IAmazonS3>();
                services.AddScoped<IImageUploadService, ImageUploadService>();
                services.AddScoped<IUploadFileService, UploadFileService>();
                services.AddScoped<IS3Client, S3Client>();
                services.AddScoped<IS3AzureClient, S3AzureClient>();
            }

            //  SmtpConfiguration
            EmailConfigBootStrapper.AddServices(services, Configuration);

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            // DI
            services.AddSingleton<IPaymentServices, PaymentServices>();
            //var stripeConfig = Configuration.GetSection("Stripe").Get<PaymentConfig>();
            //services.AddSingleton(stripeConfig);
            services.AddScoped<IFileLogger, FileLogger>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IGroupService, GroupService>();
            
            services.AddScoped<ICommonHelper, CommonHelper>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddSingleton<ISMSHelper, SMSHelper>();
            
            Bootstrapper.ConfigureServices(services, defaultConnection);
        }
    }
}
