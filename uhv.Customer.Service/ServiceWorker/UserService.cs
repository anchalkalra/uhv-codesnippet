﻿using AutoMapper;
using Common.Enum;
using Common.Enum.StorageEnum;
using Common.Model;
using Core.Storage.ServiceWorker.IMediaUploadServices;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Utilities;
using Utilities.EmailHelper;
using Utilities.PBKDF2Hashing;
using Utility;
using uhv.Customer.Model;
using uhv.Customer.Model.Model;
using uhv.Customer.Model.Model.StorageModel;
using uhv.Customer.Service.Infrastructure.DataModels;
using uhv.Customer.Service.Infrastructure.Repository;
using REGEX = System.Text.RegularExpressions;
using User = uhv.Customer.Service.Infrastructure.DataModels.Users;
using UserRight = uhv.Customer.Service.Infrastructure.DataModels.UserRights;

namespace uhv.Customer.Service.ServiceWorker
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly IUserRepository usersRepository;

        private readonly IUserRightsRepository usersRightsRepository;

        private readonly IUserGroupMappingRepository usersGroupMappingRepository;

        private readonly IGroupRepository groupsRepository;

        private readonly IMapper mapper;

        private readonly ICommonHelper commonHelper;

        private readonly IEmailHelper emailHelper;

        private readonly ResetPasswordConfig resetPasswordConfig;

        private readonly ISMSHelper smsHelper;
        
        private readonly SMSConfig smsConfig;

        private readonly SendVerificationMailConfig sendVerificationMailConfig;
        private readonly IImageUploadService imageUploadService;

        private readonly CustomerBaseUrlsConfig urls;

        /// <summary>
        ///  Load Authentication configuration
        /// </summary>
        private readonly AuthenticationConfig authenticationConfig;
        /// <summary>
        /// Users Service constructor to Inject dependency
        /// </summary>
        /// <param name="unitOfWork"> unit of work member repository</param>
        /// <param name="usersRepository"> user repository for user data</param>
        public UserService(IUnitOfWork unitOfWork, IUserRepository usersRepository,
            IUserRightsRepository usersRightsRepository, IUserGroupMappingRepository userGroupMappingRepository,
            IGroupRepository objIGroupRepository, ISMSHelper smsHelper, IOptions<SMSConfig> smsConfig, 
            IOptions<AuthenticationConfig> authenticationConfig, IMapper mapper, ICommonHelper commonHelper,
            IEmailHelper emailHelper, IOptions<ResetPasswordConfig> resetPasswordConfig, IOptions<SendVerificationMailConfig> sendVerificationMailConfig, IImageUploadService imageUploadService
            , IOptions<CustomerBaseUrlsConfig> config
            )
        {
            Check.Argument.IsNotNull(nameof(unitOfWork), unitOfWork);
            Check.Argument.IsNotNull(nameof(usersRepository), usersRepository);
            Check.Argument.IsNotNull(nameof(resetPasswordConfig), resetPasswordConfig);
            Check.Argument.IsNotNull(nameof(commonHelper), commonHelper);
            Check.Argument.IsNotNull(nameof(emailHelper), emailHelper);
            Check.Argument.IsNotNull(nameof(smsHelper), smsHelper);
            Check.Argument.IsNotNull(nameof(smsConfig), smsConfig);
            Check.Argument.IsNotNull(nameof(authenticationConfig), authenticationConfig);
            Check.Argument.IsNotNull(nameof(sendVerificationMailConfig), sendVerificationMailConfig);
            Check.Argument.IsNotNull(nameof(imageUploadService), imageUploadService);

            this.unitOfWork = unitOfWork;
            this.usersRepository = usersRepository;
            this.usersRightsRepository = usersRightsRepository;
            this.usersGroupMappingRepository = userGroupMappingRepository;
            this.mapper = mapper;
            this.commonHelper = commonHelper;
            this.emailHelper = emailHelper;
            this.resetPasswordConfig = resetPasswordConfig.Value;
            this.groupsRepository = objIGroupRepository;
            this.smsHelper = smsHelper;
            this.smsConfig = smsConfig.Value;
            this.authenticationConfig = authenticationConfig.Value;
            this.sendVerificationMailConfig = sendVerificationMailConfig.Value;
            this.imageUploadService = imageUploadService;
            this.urls = config.Value;
        }

        /// <summary>
        /// Get User Profile
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        public async Task<ResponseResult<UserProfileModel>> GetUserProfile(long accountId, long userId)
        {
            var user = await this.usersRepository.GetUserByUserId(accountId, userId);
            if (user == null)
            {
                return new ResponseResult<UserProfileModel>()
                {
                    Message = "Invalid User Details",
                    ResponseCode = ResponseCode.NoRecordFound,
                    Error = new ErrorResponseResult()
                    {
                        Message = ResponseMessage.NoRecordFound,
                    }
                };
            }

            return new ResponseResult<UserProfileModel>()
            {
                Message = ResponseMessage.RecordFetched,
                ResponseCode = ResponseCode.RecordFetched,
                Data = new UserProfileModel()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    DOB = user.DateOfBirth,
                    MobileCode = user.MobileCode,
                    Mobile = user.Mobile,
                    EmailAddress = user.EmailAddress,
                    IsEmailVerified = user.IsEmailVerified,
                    DrivingLicense = user.DrivingLicense,
                    ImagePath = $@"{this.urls.StorageRootPath}{user.ImagePath}"
                }
            };
        }

        /// <summary>
        /// Create new User
        /// </summary>
        /// <param name="user">user object</param>
        /// <returns></returns>
        public async Task<ResponseResult<UserVM>> CreateUser(CreateUserModel model, long accountId, long loggedInUserId, string deviceId)
        {
            // Validate Model
            var errorDetails = new Dictionary<string, string[]>();
            if (string.IsNullOrWhiteSpace(model.FirstName))
            {
                errorDetails.Add("firstName", new string[] { "This field may not be blank." });
            }
            else if (model.FirstName.Length > 50)
            {
                errorDetails.Add("firstName", new string[] { "Ensure this field has no more than 50 characters." });
            }
            
            if (string.IsNullOrWhiteSpace(model.Mobile))
            {
                errorDetails.Add("mobile", new string[] { "This field may not be blank." });
            }
            else if (await this.usersRepository.GetUserByUserMobile(model.Mobile, accountId) != null)
            {
                errorDetails.Add("mobile", new string[] { "This mobile number is already being used for another user." });
            }

            if (string.IsNullOrWhiteSpace(model.EmailAddress))
            {
                errorDetails.Add("emailAddress", new string[] { "This field may not be blank." });
            }
            else if (await this.usersRepository.GetUserByEmailId(model.EmailAddress, accountId) != null)
            {
                errorDetails.Add("emailAddress", new string[] { "This email address is already being used for another user." });
            }

            if (errorDetails.Count > 0)
            {
                return new ResponseResult<UserVM>()
                {
                    Message = ResponseMessage.ValidationFailed,
                    ResponseCode = ResponseCode.ValidationFailed,
                    Error = new ErrorResponseResult()
                    {
                        Detail = errorDetails,
                        Message = ResponseMessage.ValidationFailed,
                    }
                };
            } 

            var userModel = new Users()
            {
                AccountId = accountId,
                UserName = model.EmailAddress,
                EmailAddress = model.EmailAddress,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Mobile = model.Mobile,
                MobileCode = model.MobileCode,
                UserStatus = 1,
                FailedLoginAttempts = 0,
                IsMobileNumberVerified = true,
                IsEmailVerified = false,
                CreatedOn = DateTime.UtcNow,
                CreatedBy = loggedInUserId,
                IntreastedInLanding = model.IntreastedInLanding,
                IntreastedInRenting = model.IntreastedInRenting,
                ExternalUserId = model.ExternalUserId,
                AuthenticationCategory = model.AuthenticationCategory,
                DeviceId = deviceId
            };

            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                PBKDF2Cryptography cryptography = new PBKDF2Cryptography();
                var hashAccount = cryptography.CreateHash(model.Password);
                userModel.PasswordHash = hashAccount.Hash;
                userModel.PasswordSalt = hashAccount.Salt;
            }

            var user = await this.usersRepository.CreateUser(userModel);

            if (user == null)
            {
                return new ResponseResult<UserVM>()
                {
                    Message = ResponseMessage.InternalServerError,
                    ResponseCode = ResponseCode.InternalServerError,
                    Error = new ErrorResponseResult()
                    {
                        Message = ResponseMessage.InternalServerError
                    }
                };
            }

            if (model.UserRights != null && model.UserRights.Count > 0)
            {
                var userRights = model.UserRights.Select(x =>
                        new UserRights()
                        {
                            AccountId = accountId,
                            UserRightId = x.UserRightId,
                            ModuleId = x.ModuleId,
                            UserId = user.UserId,
                            IsPermission = x.IsPermission,
                            CreatedOn = DateTime.UtcNow,
                            CreatedBy = loggedInUserId
                        }
                    ).ToList();
                await usersRightsRepository.AddList(userRights, 0);
            }

            // Add Default User Group
            if (!string.IsNullOrWhiteSpace(authenticationConfig.DefaultUserGroup) 
                && (model.UserRights == null || model.UserRights.Count < 1) 
                && (model.Groups == null || model.Groups.Count < 1))
            {
                var group = await this.groupsRepository.GetGroupByName(authenticationConfig.DefaultUserGroup, accountId);

                if (group == null || group.GroupId < 1)
                {
                    return new ResponseResult<UserVM>()
                    {
                        Message = "Customer group is missing",
                        ResponseCode = ResponseCode.InternalServerError,
                        Error = new ErrorResponseResult()
                        {
                            Message = ResponseMessage.InternalServerError,
                        }
                    };
                }
                model.Groups = new List<long>();
                model.Groups.Add(group.GroupId);
            }

            if (model.Groups != null && model.Groups.Count > 0)
            {
                var groups = model.Groups.Select(x =>
                       new UserGroupMappings()
                       {
                           AccountId = accountId,
                           GroupId = x,
                           UserId = user.UserId,
                           CreatedOn = DateTime.UtcNow,
                           CreatedBy = loggedInUserId,
                       }
                   ).ToList();
                await usersGroupMappingRepository.AddList(groups, 0);
            }
            this.unitOfWork.Commit();

            var response = new UserVM();
            response = mapper.Map<UserVM>(model);
            response.UserId = user != null ? user.UserId : 0;
            return new ResponseResult<UserVM>()
            {
                Message = ResponseMessage.RecordSaved,
                ResponseCode = ResponseCode.RecordSaved,
                Data = response
            };
        }

        /// <summary>
        /// To Update existing User
        /// </summary>
        /// <param name="user">user object</param>
        /// <returns></returns>
        public async Task<ResponseResult<UserVM>> UpdateUser(long userId, long accountId, UsersModel model, long loggedInUserId)
        {
            var user = await this.usersRepository.GetUser(accountId, userId);
            if (user == null)
            {
                return new ResponseResult<UserVM>()
                {
                    Message = ResponseMessage.NoRecordFound,
                    ResponseCode = ResponseCode.NoRecordFound,
                    Data = null
                };
            }

            if (model.DefaultDashboardId.HasValue)
            {

              //  user.DefaultDashboardId = model.DefaultDashboardId.Value;
                user.UpdatedBy = loggedInUserId;
                user.UpdatedOn = DateTime.UtcNow;

                await this.usersRepository.UpdateUser(user);
                this.unitOfWork.Commit();
                var userResponse = mapper.Map<UserVM>(model);
                userResponse.UserId = user != null ? user.UserId : 0;
                return new ResponseResult<UserVM>()
                {
                    Message = ResponseMessage.RecordSaved,
                    ResponseCode = ResponseCode.RecordSaved,
                    Data = userResponse
                };
            }


            // Validate Model
            var errorDetails = new Dictionary<string, string[]>();
            if (string.IsNullOrWhiteSpace(model.UserName))
            {
                errorDetails.Add("userName", new string[] { "This field may not be blank." });
            }
            else if (model.UserName.Length > 255)
            {
                errorDetails.Add("userName", new string[] { "Ensure this field has no more than 255 characters." });
            }

            if (string.IsNullOrWhiteSpace(model.FirstName))
            {
                errorDetails.Add("firstName", new string[] { "This field may not be blank." });
            }
            else if (model.FirstName.Length > 100)
            {
                errorDetails.Add("firstName", new string[] { "Ensure this field has no more than 100 characters." });
            }

            var existingUser = await this.usersRepository.GetUserByUserName(model.UserName, accountId);
            if (existingUser != null && existingUser.UserId != userId)
            {
                errorDetails.Add("userName", new string[] { "This user name is already being used for another user." });
            }
            //addded by Raj for validate Email and Mobile
            var existingEmail = await this.usersRepository.GetUserByEmailId(model.EmailAddress, accountId);
            if (existingEmail != null && existingEmail.UserId != userId)
            {
                errorDetails.Add("emailAddress", new string[] { "This email address is already being used for another user." });
            }
            var existingMobile = await this.usersRepository.GetUserByUserMobile(model.Mobile, accountId);
            if (existingMobile != null && existingMobile.UserId != userId)
            {
                errorDetails.Add("mobile", new string[] { "This mobile is already being used for another user." });
            }
            if (errorDetails.Count > 0)
            {
                return new ResponseResult<UserVM>()
                {
                    Message = ResponseMessage.ValidationFailed,
                    ResponseCode = ResponseCode.ValidationFailed,
                    Error = new ErrorResponseResult()
                    {
                        Detail = errorDetails,
                        Message = ResponseMessage.ValidationFailed,
                    }
                };
            }

            UserVM response = new UserVM();


            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.EmailAddress = model.EmailAddress;
            user.Mobile = model.Mobile;
            user.UserName = model.UserName;
            user.UserStatus = (short)model.UserStatus;
            user.UpdatedBy = loggedInUserId;
            user.UpdatedOn = DateTime.UtcNow;

            await this.usersRepository.UpdateUser(user);

            // Group Rights
            var groupsRights = new List<long>();
            // Fetch data for user modules according to usergroup mapping
            List<long> userModuleNavigation = await this.usersRepository.GetUserGroupRights(userId, accountId);
            if (userModuleNavigation.Count > 0)
            {
                groupsRights = userModuleNavigation;
            }

            // User Groups
            var groups = new List<UserGroupMappings>();
            if (model.Groups != null && model.Groups.Count > 0)
            {
                groups = model.Groups.Select(x =>
                       new UserGroupMappings()
                       {
                           AccountId = accountId,
                           GroupId = x,
                           UserId = user.UserId,
                           CreatedOn = DateTime.UtcNow,
                           UpdatedOn = DateTime.UtcNow,
                           CreatedBy = loggedInUserId,
                           UpdatedBy = loggedInUserId
                       }
                   ).ToList();


            }
            await usersGroupMappingRepository.AddList(groups, user.UserId);

            // Update UserRights
            var userRights = new List<UserRight>();
            if (model.UserRights != null && model.UserRights.Count > 0)
            {
                userRights = model.UserRights.Select(x =>
                        new UserRight()
                        {
                            AccountId = accountId,
                            UserRightId = x.UserRightId,
                            ModuleId = x.ModuleId,
                            UserId = user.UserId,
                            IsPermission = x.IsPermission,
                            CreatedOn = DateTime.UtcNow,
                            UpdatedOn = DateTime.UtcNow,
                            CreatedBy = loggedInUserId,
                            UpdatedBy = loggedInUserId
                        }
                    ).ToList();
            }

            var finalUserRights = userRights;


            foreach (var objgroupsRights in groupsRights)
            {
                var checkModuleRightExist = userRights.FirstOrDefault(x => x.ModuleId == objgroupsRights);
                if (checkModuleRightExist == null)
                {
                    finalUserRights.Add(
                        new UserRight()
                        {
                            AccountId = accountId,
                            ModuleId = objgroupsRights,
                            UserId = user.UserId,
                            IsPermission = false,
                            CreatedOn = DateTime.UtcNow,
                            UpdatedOn = DateTime.UtcNow,
                            CreatedBy = loggedInUserId,
                            UpdatedBy = loggedInUserId
                        });
                }
            }

            await usersRightsRepository.AddList(finalUserRights, user.UserId);

            this.unitOfWork.Commit();


            response = mapper.Map<UserVM>(model);
            response.UserId = user != null ? user.UserId : 0;
            return new ResponseResult<UserVM>()
            {
                Message = ResponseMessage.RecordSaved,
                ResponseCode = ResponseCode.RecordSaved,
                Data = response
            };
        }

        /// <summary>
        /// To delete existing User
        /// </summary>
        /// <param name="userId">user identifier</param>
        /// <returns></returns>
        public async Task<long> DeleteUser(long userId)
        {
            await this.usersRightsRepository.Delete(userId);
            await this.usersGroupMappingRepository.Delete(userId);
            return await this.usersRepository.DeleteUser(userId);
        }

        /// <summary>
        /// Set Password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseResult<SuccessMessageModel>> SetPassword(SetPasswordModel model, long loggedInUserId)
        {
            var response = new ResponseResult<SuccessMessageModel>();

            // Validate required fields
            if (model == null)
            {
                response.Message = ResponseMessage.ValidationFailed;
                response.ResponseCode = ResponseCode.ValidationFailed;
                response.Error = new ErrorResponseResult()
                {
                    Message = ResponseMessage.ValidationFailed
                };
                return response;
            }

            var errors = new Dictionary<string, string[]>();
            if (string.IsNullOrWhiteSpace(model.Password))
            {
                errors.Add("password", new string[] { "This field may not be blank." });
            }

            if (string.IsNullOrWhiteSpace(model.Uid))
            {
                errors.Add("uid", new string[] { "This field may not be blank." });
            }

            if (string.IsNullOrWhiteSpace(model.Token))
            {
                errors.Add("token", new string[] { "This field may not be blank." });
            }

            if (errors.Count > 0)
            {
                response.Message = ResponseMessage.ValidationFailed;
                response.ResponseCode = ResponseCode.ValidationFailed;
                response.Error = new ErrorResponseResult()
                {
                    Message = ResponseMessage.ValidationFailed,
                    Detail = errors
                };
                return response;
            }

            // Decrypt Token & UserId
            var uid = HttpUtility.UrlDecode(commonHelper.DecryptString(model.Uid, resetPasswordConfig.EncryptKey));
            var token = HttpUtility.UrlDecode(commonHelper.DecryptString(model.Token, resetPasswordConfig.EncryptKey));

            if (string.IsNullOrWhiteSpace(uid) || string.IsNullOrWhiteSpace(token)
                || !Int64.TryParse(uid, out var userId))
            {
                response.Message = ResponseMessage.ValidationFailed;
                response.ResponseCode = ResponseCode.ValidationFailed;
                response.Error = new ErrorResponseResult()
                {
                    Message = ResponseMessage.ValidationFailed
                };
                return response;
            }

            var tokenValues = token.Split("~!|");

            // Validate Token values
            if (tokenValues.Length < 4 || !Int64.TryParse(tokenValues[0], out var tokenUserId)
                || tokenUserId != userId || !Int64.TryParse(tokenValues[1], out var accountId)
                || accountId < 1 || !Int64.TryParse(tokenValues[2], out var lastUpdatedOn)
                || !DateTime.TryParse(tokenValues[3], out var tokenCreatedOn))
            {
                response.Message = ResponseMessage.Unauthorized;
                response.ResponseCode = ResponseCode.Unauthorized;
                response.Error = new ErrorResponseResult()
                {
                    Message = ResponseMessage.Unauthorized
                };
                return response;
            }

            // Validate Token date
            if (tokenCreatedOn.AddHours(resetPasswordConfig.ExpiryTimeInHours) < DateTime.Now)
            {
                response.Message = "Reset Password link has been expired.";
                response.ResponseCode = ResponseCode.Unauthorized;
                response.Error = new ErrorResponseResult()
                {
                    Message = "Reset Password link has been expired."
                };
                return response;
            }

            // Validate User Last Updated On date
            var user = await usersRepository.GetUserByUserId(accountId, userId);

            if (user == null)
            {
                response.Message = "Reset Password link has been expired.";
                response.ResponseCode = ResponseCode.Unauthorized;
                response.Error = new ErrorResponseResult()
                {
                    Message = "Reset Password link has been expired."
                };
                return response;
            }

            // Validate last updated date
            var userLastUpdatedDate = user.CreatedOn;
            if (user.UpdatedOn != null)
            {
                userLastUpdatedDate = (DateTime)user.UpdatedOn;
            }

            if (lastUpdatedOn != userLastUpdatedDate.Ticks)
            {
                response.Message = "Reset Password link has been expired.";
                response.ResponseCode = ResponseCode.Unauthorized;
                response.Error = new ErrorResponseResult()
                {
                    Message = "Reset Password link has been expired."
                };
                return response;
            }

            // Validate New Password
            var cryptography = new PBKDF2Cryptography();
            var validationErrorMsg = await ValidatePassword(cryptography, user.AccountId, user.UserId, model.Password, false, null);

            if (!string.IsNullOrWhiteSpace(validationErrorMsg))
            {
                response.Message = validationErrorMsg;
                response.ResponseCode = ResponseCode.InternalServerError;
                response.Error = new ErrorResponseResult()
                {
                    Message = validationErrorMsg
                };
                return response;
            }

            //Change Password
            var hashAccount = cryptography.CreateHash(model.Password);
            user.PasswordHash = hashAccount.Hash;
            user.PasswordSalt = hashAccount.Salt;
            user.UpdatedOn = DateTime.UtcNow;
            user.UpdatedBy = loggedInUserId;
            unitOfWork.Commit();

            response.Message = "Password reset complete.";
            response.ResponseCode = ResponseCode.RecordSaved;
            response.Data = new SuccessMessageModel()
            {
                Message = "Password reset complete."
            };
            return response;
        }
        private async Task<string> ValidatePassword(PBKDF2Cryptography cryptography, long account, long userId, string password, bool isCalledForDefaultUser, PasswordPolicyModel objPasswordPolicyModel)
        {
            // Get Password Policy.
            PasswordPolicyModel passwordPolicy = new PasswordPolicyModel();
            if (isCalledForDefaultUser && objPasswordPolicyModel != null)
            {
                // this is for default user setup
                passwordPolicy = objPasswordPolicyModel;
            }
            // If Password Policy not found for account, return error.
            if (passwordPolicy == null)
            {
                return ResponseMessage.InternalServerError;
            }

            // If User Allowed To Change Password, return error.
            if (!passwordPolicy.AllowUsersToChangePassword)
            {
                return ResponseMessage.Unauthorized;
            }

            // Check minimum Password Length
            if (passwordPolicy.MinPasswordLength > 0
                && password.Length < passwordPolicy.MinPasswordLength)
            {
                return "Password Length is less than " + passwordPolicy.MinPasswordLength;
            }

            // Check One lower case character in Password
            if (passwordPolicy.OneLowerCase
                && password.Where(char.IsLower).Count() < 1)
            {
                return "Password does not have any lower case character.";
            }

            // Check One Upper case character in Password
            if (passwordPolicy.OneUpperCase
                && password.Where(char.IsUpper).Count() < 1)
            {
                return "Password does not have any Upper case character.";
            }

            // Check One number in Password
            if (passwordPolicy.OneNumber
                && password.Where(char.IsNumber).Count() < 1)
            {
                return "Password does not have any number.";
            }

            // Check One special character in Password
            if (passwordPolicy.OneSpecialChar
                && !REGEX.Regex.IsMatch(password, @"[(\@|\$|\~|\#|\&|\^|\!)]"))
            {
                return "Password does not have any special character.";
            }
            return "";
        }
    }
}