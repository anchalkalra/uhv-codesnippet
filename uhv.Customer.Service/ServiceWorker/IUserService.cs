﻿using uhv.Customer.Service.Infrastructure.DataModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using uhv.Customer.Model;
using Common.Model;
using User = uhv.Customer.Service.Infrastructure.DataModels.Users;

namespace uhv.Customer.Service.ServiceWorker
{
    public interface IUserService
    {

        /// <summary>
        /// Set Password of User
        /// </summary> 
        /// <param name="model">Set Password Request Model</param>
        /// <returns>return response result</returns>
        Task<ResponseResult<SuccessMessageModel>> SetPassword(SetPasswordModel model, long loggedInUserId);

        /// <summary>
        /// Update User Profile
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        Task<ResponseResult<UserProfileModel>> GetUserProfile(long accountId, long userId);

        Task<ResponseResult<UserVM>> CreateUser(CreateUserModel model, long accountId, long loggedInUserId, string deviceId);

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="accountId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseResult<UserVM>> UpdateUser(long userId, long accountId, UsersModel model, long loggedInUserId);

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<long> DeleteUser(long userId);


    }
}
