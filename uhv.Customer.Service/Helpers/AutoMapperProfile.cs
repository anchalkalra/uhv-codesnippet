﻿using AutoMapper;
using uhv.Admin.Model;
using uhv.Customer.Model;
using uhv.Customer.Model.Model;
using uhv.Customer.Model.Model.Reservation;
using uhv.Customer.Service.Infrastructure.DataModels;
using DataModel = uhv.Customer.Service.Infrastructure.DataModels;
using DomainModel = uhv.Customer.Model;

namespace uhv.Customer.Service.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Groups, DomainModel.GroupsModel>();
            CreateMap<GroupRights, DomainModel.GroupRightsModel>();
            
            CreateMap<DataModel.Users, DomainModel.UsersModel>();
            CreateMap<DomainModel.UsersModel, DataModel.Users>();
            CreateMap<DomainModel.UserVM, DomainModel.UsersModel>();
            CreateMap<DomainModel.UsersModel, DomainModel.UserVM>();
            

           
        }
    }   
}
