﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace uhv.Customer.Service.Infrastructure.DataModels
{
    public partial class CloudAcceleratorContext : DbContext
    {
        public readonly string schemaId;
        public CloudAcceleratorContext(DbContextOptions options, string schema = "customer")
            : base(options)
        {
            this.schemaId = schema ?? "customer";
        }

       
        public virtual DbSet<GroupRights> GroupRights { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
       
        public virtual DbSet<UserGroupMappings> UserGroupMappings { get; set; }
        public virtual DbSet<UserRights> UserRights { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=172.29.17.138;Database=Db;Username=postgres;Password=*******");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<GroupRights>(entity =>
            {
                entity.HasKey(e => e.GroupRightId)
                    .HasName("GrpModule_Key");

                entity.ToTable("GroupRights", schemaId);

                entity.Property(e => e.GroupRightId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedOn).HasColumnType("timestamp with time zone");

                entity.Property(e => e.UpdatedOn).HasColumnType("timestamp with time zone");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupRights)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_GroupRights");
            });

            modelBuilder.Entity<Groups>(entity =>
            {
                entity.HasKey(e => e.GroupId)
                    .HasName("Groups_pkey");

                entity.ToTable("Groups", schemaId);

                entity.HasIndex(e => new { e.Name, e.AccountId })
                    .HasName("UQ_Name_Account")
                    .IsUnique();

                entity.Property(e => e.GroupId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedOn).HasColumnType("timestamp with time zone");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedOn).HasColumnType("timestamp with time zone");
            });

            
            modelBuilder.Entity<UserGroupMappings>(entity =>
            {
                entity.HasKey(e => e.UserGroupMappingId)
                    .HasName("UsersGroupsMapping_pkey");

                entity.ToTable("UserGroupMappings", schemaId);

                entity.Property(e => e.UserGroupMappingId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedOn).HasColumnType("timestamp with time zone");

                entity.Property(e => e.UpdatedOn).HasColumnType("timestamp with time zone");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.UserGroupMappings)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UsersGroupMapping_Groups_GroupId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserGroupMappings)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UsersGroupMapping_Users_UserId");
            });

            modelBuilder.Entity<UserRights>(entity =>
            {
                entity.HasKey(e => e.UserRightId)
                    .HasName("UserModule_pkey");

                entity.ToTable("UserRights", schemaId);

                entity.Property(e => e.UserRightId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedOn).HasColumnType("timestamp with time zone");

                entity.Property(e => e.UpdatedOn).HasColumnType("timestamp with time zone");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRights)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UsersRights__Users__UserId");
            });

            

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("Users_pkey");

                entity.ToTable("Users", schemaId);

                entity.HasIndex(e => e.AccountId)
                    .HasName("fki_Users_FK");

                entity.HasIndex(e => e.ParkingProvidersLocationId)
                    .HasName("fki_User_ParkingProvidersLocationId_FK");

                entity.HasIndex(e => new { e.AccountId, e.UserName })
                    .HasName("username_unique")
                    .IsUnique();

                entity.Property(e => e.UserId)
                    .HasIdentityOptions(null, null, null, 9999999L, null, null)
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.AuthenticationCategory).HasMaxLength(10);

                entity.Property(e => e.CreatedOn).HasColumnType("timestamp with time zone");

                entity.Property(e => e.DateOfBirth).HasColumnType("timestamp with time zone");

                entity.Property(e => e.DeviceId).HasMaxLength(500);

                entity.Property(e => e.DrivingLicense).HasMaxLength(100);

                entity.Property(e => e.EmailAddress)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ExternalUserId).HasMaxLength(500);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ImagePath).HasMaxLength(4000);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Mobile).HasMaxLength(13);

                entity.Property(e => e.MobileCode).HasMaxLength(4);

                entity.Property(e => e.PasswordExpirationDate).HasColumnType("timestamp with time zone");

                entity.Property(e => e.StripeCustomerId).HasMaxLength(200);

                entity.Property(e => e.UpdatedOn).HasColumnType("timestamp with time zone");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
