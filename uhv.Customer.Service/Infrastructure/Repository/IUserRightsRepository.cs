﻿using System.Collections.Generic;
using System.Threading.Tasks;
using uhv.Customer.Service.Infrastructure.DataModels;

namespace uhv.Customer.Service.Infrastructure.Repository
{
    public interface IUserRightsRepository
    {
        Task<UserRights> Add(UserRights userRight);
        Task<List<UserRights>> AddList(List<UserRights> userRights, long userId);
        Task<bool> Delete(long userId);
    }
}
