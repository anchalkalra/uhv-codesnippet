﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using uhv.Customer.Service.Infrastructure.DataModels;

namespace uhv.Customer.Service.Infrastructure.Repository
{
    public static  class Bootstrapper
    {
        public static string defaultConnectionString;
        static Bootstrapper()
        {

        }

        public static void ConfigureServices(this IServiceCollection services, string defaultConnection= "Server=*****; Database=*****; user id=*****; password=*****;")
        {
            defaultConnectionString = defaultConnection;
            services.AddScoped<IGroupRepository, GroupRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserRightsRepository, UserRightsRepository>();
            services.AddScoped<IUserGroupMappingRepository, UserGroupMappingRepository>();
            
            //services.AddScoped<ILoginHistoryRepository, LoginHistoryRepository>();
            //services.AddScoped<IRefreshTokenRepository, RefreshTokenRepository>();
           
        }
    }
}
