﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace uhv.Admin.API
{
    public class Program
    {

        /// <summary>
        /// Main method of application to start 
        /// </summary>
        /// <param name="args">args is a string array</param>
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
