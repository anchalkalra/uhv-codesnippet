﻿using System;
using System.Collections.Generic;
using System.Text;

namespace uhv.Customer.Model
{
    public class AnalyticVM
    {
        public DateTime Date { get; set; }
        public long Count { get; set; }
    }
    public class DayWiseAnalyticVM
    {
        public List<DateTime> Date { get; set; }
        public List<long> IncomingCount { get; set; }
        public List<long> OutgoingCount { get; set; }
    }
}
