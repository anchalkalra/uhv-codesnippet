﻿using System;
using System.Collections.Generic;
using System.Text;

namespace uhv.Customer.Model
{
    public class VerifyEmailVM
    {
        public string Token { get; set; }
        public string Uid { get; set; }
    }
}
