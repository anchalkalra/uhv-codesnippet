﻿using System;
using System.Collections.Generic;
using System.Text;
using uhv.Customer.Model.Model;

namespace uhv.Customer.Model
{
    public class EmailParseDetails
    {
        public long EmailParseDetailId { get; set; }
        public Guid MessageId { get; set; }
        public string RequestIds { get; set; }
        public long UserId { get; set; }
    }
}
