﻿using System;
using System.Collections.Generic;
using System.Text;

namespace uhv.Customer.Model.VinDecoder
{
    public class VINDecoderVM
    {
        public string Id { get; set; }
        public string VIN { get; set; }
    }
}
