﻿using uhv.Customer.Model.Model.StorageModel;

namespace uhv.Customer.Model
{
    public class UserProfileImageVM
    {
        public Document ProfileDocument { get; set; }
    }
}
