﻿using System;
using System.Collections.Generic;
using System.Text;

namespace uhv.Customer.Model.Model
{
    public class ScannedResponseVM
    {
        public string CurrentActivityCode  { get; set; }
        public string NextActivityCode { get; set; }
    }
}
