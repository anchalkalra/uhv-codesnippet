﻿using System;
using System.Collections.Generic;
using System.Text;

namespace uhv.Customer.Model
{
    public class AuthenticationConfigKeyValueResultModel : AuthenticationConfigKeyValueModel
    {
        public string KeyName { get; set; }
    }
}
