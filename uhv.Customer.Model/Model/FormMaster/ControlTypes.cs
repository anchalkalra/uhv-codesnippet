﻿using System;
using System.Collections.Generic;
using System.Text;

namespace uhv.Customer.Model
{
    public partial class ControlTypes
    {
        public int ControlTypeId { get; set; }
        public string ControlType { get; set; }
    }
}
