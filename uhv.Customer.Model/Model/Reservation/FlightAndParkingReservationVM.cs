﻿using System;
using System.Collections.Generic;

namespace uhv.Customer.Model.Model.Reservation
{

    public class FlightAndParkingReservationVM
    {
        public long ReservationId { get; set; }
        public string ReservationCode { get; set; }
        public long UserId { get; set; }
        public FlightReservationVM FlightReservation { get; set; }
        public ParkingReservationVM ParkingReservation { get; set; }
        public ReservationVehicleVM ReservationVehicle { get; set; }
    }
}
