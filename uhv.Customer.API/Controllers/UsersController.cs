﻿using Logger;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Utilities;
using uhv.Customer.Model;
using uhv.Customer.Service.ServiceWorker;

namespace uhv.Customer.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : BaseApiController
    {
        private readonly IUserService userService;

        /// <summary>
        /// Users Controller constructor to Inject dependency
        /// </summary>
        /// <param name="userService">user service class</param>
        public UsersController(IUserService userService, IFileLogger logger) : base(logger: logger)
        {
            Check.Argument.IsNotNull(nameof(userService), userService);
            this.userService = userService;
        }

        // POST api/<AccountsController> 
        /// <summary>
        /// Set Password of User
        /// </summary>
        /// <param name="model">Set Password Request Model</param>
        /// <returns>return response result</returns>
        [Route("setPassword")]
        [HttpPost]
        public async Task<IActionResult> SetPassword(SetPasswordModel model)
        {
            return await Execute(async () =>
            {
                var result = await this.userService.SetPassword(model, loggedInUserId);
                return Ok(result);
            });
        }

        [Route("")]
        /// <summary>
        /// This api is used for Creating new User
        /// </summary>
        /// <param name="user">The new user object.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] CreateUserModel model)
        {
            return await Execute(async () =>
            {
                var result = await this.userService.CreateUser(model, loggedInAccountId, loggedInUserId, deviceId);
                return Ok(result);
            });
        }

        /// <summary>
        /// This api is used for Updating existing User
        /// </summary>
        /// <param name="model">The existing user object.</param>
        /// <param name="userId">A unique integer value identifying this User.</param>
        /// <returns></returns>
        [Route("{userId}")]
        [HttpPut]
        public async Task<IActionResult> UpdateUser([FromRoute] long userId, [FromBody] UsersModel model)
        {
            return await Execute(async () =>
            {
                var result = await this.userService.UpdateUser(userId, loggedInAccountId, model, loggedInUserId);
                return Ok(result);
            });

        }

        /// <summary>
        /// This api is used for deleing User
        /// </summary>
        /// <param name="userId">user identifier</param>
        /// <returns></returns>
        [Route("{userId}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteUser([FromRoute] long userId)
        {
            return await Execute(async () =>
            {
                var result = await this.userService.DeleteUser(userId);
                return Ok(result);
            });
        }


    }
}
