﻿using System;
using System.Collections.Generic;
using System.Text;

namespace uhv.Admin.Model
{
    public class RefreshTokenRequestModel
    {
        public string Refresh { get; set; }
    }
}
