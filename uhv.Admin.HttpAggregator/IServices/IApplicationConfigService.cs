﻿using Common.Model;
using uhv.Admin.Model;
using System.Threading.Tasks;

namespace uhv.Admin.HttpAggregator.IServices
{
    /// <summary>
    /// ApplicationConfig Service Class
    /// </summary>
    public interface IApplicationConfigService
    {
        ///<summary>
        /// Get Application Config Details
        ///</summary>
        ///<returns></returns>
        Task<ResponseResult<ApplicationConfig>> GetApplicationConfig();

        ///<summary>
        /// Get Database Details
        ///</summary>
        ///<returns></returns>
        Task<ResponseResult<DatabaseDetails>> GetDatabaseDetails();
    }
}
