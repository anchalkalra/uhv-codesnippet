﻿using uhv.Admin.Service.Infrastructure.DataModels;
using System.Threading.Tasks;

namespace uhv.Admin.Service.Infrastructure.Repository
{
    public interface ILoginHistoryRepository
    {
        Task<LoginHistories> AddLoginHistory(LoginHistories loginHistory);
        Task<LoginHistories> GetLoginHistory(string token);
        Task<bool> UpdateLoginHistory(LoginHistories model);
    }
}
