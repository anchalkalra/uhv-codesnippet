﻿using uhv.Admin.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace uhv.Admin.Service.Infrastructure.Repository
{
    public interface IAuthenticationConfigKeyRepository
    {
        /// <summary>
        /// Get All Authentication Config Keys
        /// </summary>
        /// <returns></returns>
        Task<List<AuthenticationConfigKeyModel>> GetAuthenticationConfigKeys(string authenticationType);
    }
}
