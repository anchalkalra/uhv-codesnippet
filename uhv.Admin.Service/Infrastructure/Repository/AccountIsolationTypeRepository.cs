﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yoda.Admin.API.Infrastructure.DataModels; 

namespace Yoda.Admin.API.Infrastructure.Repository
{
    public class AccountIsolationTypeRepository : RepositoryBase<AccountIsolationType>, IAccountIsolationTypeRepository
    {
        public AccountIsolationTypeRepository(CloudAcceleratorContext context) : base(context)
        {
            //TODO-- further implementation
        }

        /// <summary>
        ///Get All Account Isolation Types
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AccountIsolationType>> GetAllAccountIsolationTypes()
        {
            var result = base.context.AccountIsolationType.ToList();
            return result;
        }
    }
}
