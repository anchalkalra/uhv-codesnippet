﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Yoda.Admin.API.Infrastructure.Repository
{
    public interface IAccountIsolationTypeRepository
    {
        /// <summary>
        /// Get All Account Isolation Types
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Model.AccountIsolationType>> GetAllAccountIsolationTypes();
    }
}
