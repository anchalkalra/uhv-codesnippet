﻿using uhv.Admin.Service.Infrastructure.DataModels;
using System.Threading.Tasks;

namespace uhv.Admin.Service.Infrastructure.Repository
{
    public interface IApplicationConfigRepository
    {
        Task<ApplicationConfigs> GetApplicationConfig();
    }
}
