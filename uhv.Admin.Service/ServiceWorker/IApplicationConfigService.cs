﻿using uhv.Admin.Model;
using System.Threading.Tasks;

namespace uhv.Admin.Service.ServiceWorker
{
    public interface IApplicationConfigService
    {
        /// <summary>
        /// Get Application Config Details
        /// </summary>
        /// <returns></returns>
        Task<ApplicationConfig> GetApplicationConfig();
    }
}
