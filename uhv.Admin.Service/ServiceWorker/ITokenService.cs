﻿using uhv.Admin.Service.Infrastructure.DataModels;
using uhv.Admin.Model;
using System;
using System.Threading.Tasks;

namespace uhv.Admin.Service.ServiceWorker
{
    public interface ITokenService
    {
        AuthResultModel GenerateAuthToken(AdminUsers user);
        string GenerateJwtToken(AdminUsers user, out Guid tokenId);
        string VerifyToken(RefreshTokenModel storedToken);
    }
}
