﻿using Common.Model;
using System.Threading.Tasks;
using uhv.Customer.Model;

namespace uhv.Customer.HttpAggregator.IServices.UserManagement
{
    /// <summary>
    /// Password Policy
    /// </summary>
    public interface IPasswordPolicyService
    {

        /// <summary>
        /// Get Password Policy By Account Id
        /// </summary>
        Task<ResponseResult<PasswordPolicyModel>> GetPasswordPolicyByAccountId(long accountId);

        /// <summary>
        /// Create password policy
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseResult<PasswordPolicyModel>> CreatePasswordPolicy(PasswordPolicyModel model);

        /// <summary>
        /// Update password policy
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseResult<PasswordPolicyModel>> UpdatePasswordPolicy(PasswordPolicyModel model);
    }
}
