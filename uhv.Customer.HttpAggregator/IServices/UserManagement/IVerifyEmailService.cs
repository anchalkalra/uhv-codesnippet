﻿using Common.Model;
using System.Threading.Tasks;
using uhv.Customer.Model;

namespace uhv.Customer.HttpAggregator.IServices.UserManagement
{
    /// <summary>
    /// 
    /// </summary>
    public interface IVerifyEmailService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        Task<ResponseResult<SuccessMessageModel>> VerifyEmail(string token, string uid);
    }
}
